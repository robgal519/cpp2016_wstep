#ifndef MapPoint_h
#define MapPoint_h

#include <cstring>
#include <math.h>
#include <stdlib.h>
#include <iostream>

struct MapPoint
{

	char* miasto;
	double lat;
	double lon;
};

struct MapDist
{
	double latitude;
	double longitude;
};


/**
@brief construct is adding next data to structure MapPoint
*/
MapPoint construct(const char*, const double, const double);
MapPoint construct(MapPoint, const double ,const double );
/**
@brief prints all data from structure MapPoint
*/

void print(MapPoint);


/**
@brief calculates diference betwene two point in X and in Y axis 
@return structure with distance on X, and Y axis; 
*/
MapDist distance(MapPoint, MapPoint);

/**
@return distance between two points
*/
double angularDistance(MapDist);

/**
@return the closest city to the first one 
*/
MapPoint closestFrom(MapPoint,MapPoint,MapPoint);

/**
@return name of the city
*/
char* name(MapPoint);
/**
return point betwen two points
*/
MapPoint inTheMiddle(MapPoint,MapPoint,const char*);

/**
@brief deletes cities
*/
void clear(MapPoint);
void clear(MapPoint, MapPoint);
void clear(MapPoint, MapPoint, MapPoint);
#endif