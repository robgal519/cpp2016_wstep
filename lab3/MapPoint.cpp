#include "MapPoint.h"
#include <stdio.h>
MapPoint construct(const char* name, const double lat, const double lon)
{
	MapPoint MP;

	MP.miasto=(char*)malloc(sizeof(char)*strlen(name)+1);
	strcpy(MP.miasto,name);
	MP.lat=lat;
	MP.lon=lon;

	return MP;
}
MapPoint construct(MapPoint MP, double lat, double lon)
{
	MapPoint Temp;
	const char* near="Nearby ";
	Temp.miasto=(char*) malloc(sizeof( char)*strlen(MP.miasto)+strlen(near)+1);
	sprintf(Temp.miasto,"%s%s",near,MP.miasto);
	Temp.lat=MP.lat+lat;
	Temp.lon=MP.lon+lon;
	return Temp;
}

void print(MapPoint MP)
{
	char lon[] ="E";
	char lan[] ="N";
	double longitude=MP.lon;
	double latitude=MP.lat;
	if(MP.lon<0) 
		{
			sprintf(lon,"W");
			longitude*=(-1);
		}
	if(MP.lat<0) 
		{
			sprintf(lan,"S");
			latitude*=(-1);
		}

	std::cout<<MP.miasto <<" "<<latitude<<lan<<" "<<longitude<<lon<<"\n";
}

MapDist distance(MapPoint A, MapPoint B)
{
MapDist MD;
MD.latitude=(A.lat-B.lat);
MD.longitude=(A.lat-B.lat);
return MD;
}


double angularDistance(MapDist MD)
{
	return sqrt(MD.latitude*MD.latitude+MD.longitude*MD.longitude);
}


MapPoint closestFrom(MapPoint A,MapPoint B,MapPoint C)
{
	if(angularDistance(distance(A,B))<angularDistance(distance(A,C)))
		return B;
	else
		return C;		
}

MapPoint inTheMiddle(MapPoint A, MapPoint B, const char* nazwa)
{
	MapPoint temp;
	temp.miasto=(char*)malloc(sizeof(char)*strlen(nazwa)+1);
	strcpy(temp.miasto,nazwa);
	temp.lat=(A.lat+B.lat)/2;
	temp.lon=(A.lon+B.lon)/2;
	return temp;
}

char* name(MapPoint MP)
{
	return MP.miasto;
}

void clear(MapPoint MP)
{
	free(MP.miasto);
}
void clear(MapPoint A, MapPoint B)
{
	free(A.miasto);
	free(B.miasto);
}

void clear(MapPoint A, MapPoint B, MapPoint C)
{
	free(A.miasto);
	free(B.miasto);
	free(C.miasto);

}
