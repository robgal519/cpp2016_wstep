#include <cstring>
#include "Student.h"

//Public constructors
/////////////////////////////////////////////////////////

Student::Student(const char* name1, const char* name2, const char* surname, int year)
{
	_name1 = new char[strlen(name1)+1];
	_name2 = new char[strlen(name2)+1];
	_surname = new char[strlen(surname)+1];

	strcpy(_name1, name1);
	strcpy(_name2, name2);
	strcpy(_surname, surname);
	_year = year;
}

//Public methods
/////////////////////////////////////////////////////////

char* Student::name1()const
{
	return _name1;
}

char* Student::name2()const
{
	return _name2;
}

char* Student::surname()const
{
	return _surname;
}

int Student::year()const
{
	return _year;
}

Student::~Student()
{
	delete [] _name1;
	delete [] _name2;
	delete [] _surname;
}