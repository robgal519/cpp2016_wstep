#ifndef TestObj_h
#define TestObj_h

#include <cstring>
#include <iostream>
class TestObj{
char* _data;

public:
	TestObj();
	TestObj(const char* data);
	TestObj(const TestObj& copy);
	~TestObj();

	const char* name();
};
TestObj::TestObj(){
	char  tekst[]="domniemany";
	_data= new char[sizeof(tekst)+1];
	strcpy(_data, tekst);
	std::cout<<"TestObj::TestObj "<<_data<<std::endl;
}
TestObj::TestObj(const char* data){
	_data=new char[strlen(data)+1];
	strcpy(_data,data);
	std::cout<<"TestObj::TestObj z arg char: "<<_data<<std::endl;
}
TestObj::TestObj(const TestObj& copy){
	_data=new char[strlen(copy._data)+1];
	strcpy(_data,copy._data);
	std::cout<<"TestObj::TestObjz arg TestObj: "<<_data<<std::endl;
}
TestObj::~TestObj(){
	std::cout<<"TestObj::~TestObj "<<_data<<std::endl;
	delete [] _data;
}
const char* TestObj::name()
{
	return _data;
}

#endif