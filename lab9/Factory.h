#ifndef Factory_h
#define Factory_h
#include "TestObj.h"

class Factory{
static TestObj** array;
static TestObj* prototypeObject;
static int counter;
public:
	
	static TestObj* produce();
	static TestObj* produce(const char* data);
	static TestObj* prototype(const TestObj& obj);
	static void erase();
	static void increaseArray();
	Factory();

};


TestObj* Factory::produce(){
increaseArray();
if(prototypeObject!=0)
{
	array[counter-1]=prototypeObject;
	//delete prototypeObject;
}else
	array[counter-1]=new TestObj();
return array[counter-1];

}
TestObj* Factory::produce(const char* data){
increaseArray();
array[counter-1]=new TestObj(data);
return array[counter-1];
}
TestObj* Factory::prototype(const TestObj& obj){
increaseArray();
prototypeObject=new TestObj(obj);
return array[counter-1];
}

void Factory::increaseArray(){
	TestObj** temp = new TestObj*[counter+1];
	memcpy(temp, array,(counter)*sizeof(TestObj*));
	delete[] array;
	array=temp;
	//delete[] temp;
	++counter;
}
void Factory::erase(){
	for(int i=0;i<counter;++i)
	{
		delete array[i];
	}
	//delete[] array;
	array=0;
	//delete prototypeObject;
	prototypeObject=0;
	counter = 0;
}


TestObj** Factory::array=0;
TestObj* Factory::prototypeObject=0;
int Factory::counter=0;


#endif