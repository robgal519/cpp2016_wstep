#include "Matrix.h"

Vector Matrix::extractRow(const int index)const{
	return _rowId[index];
}

Matrix& Matrix::set(const int row, const int collumn, const double value){
	this->_rowId[row].set(collumn)= value;
	return *this;
}

Vector Matrix::extractColumn(const int index)const{
	Vector temp(this->_rowId[0].at(index), _rowId[1].at(index), _rowId[2].at(index));
	return temp;

}

const Matrix Matrix::diagonal( double value){
Vector v1(value,0,0);
Vector v2(0,value,0);
Vector v3(0,0,value);
	Matrix temp(v1,v2,v3);
return temp;

}
void Matrix::print() const{
	for(int i=0;i<3;++i)
		_rowId[i].print();

}
