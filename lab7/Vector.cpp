#include "Vector.h"
#include <iostream>

double Vector::at(const int posision) const {
	if(posision<3)
		return _row[posision];
}
double& Vector::set(const int posision)
{
	return _row[posision];
}
void Vector::print()const
{
	for(int i=0;i<3;++i)
	{
		std::cout<<_row[i]<<" ";
	}
	std::cout<<std::endl;
}
