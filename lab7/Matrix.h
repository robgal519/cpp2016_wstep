#ifndef Matrix_h
#define Matrix_h
#include "Vector.h"

class Matrix{
	Vector _rowId[3];
public:
	Matrix(Vector v1, Vector v2, Vector v3):_rowId{v1,v2,v3}
	{
	};
	// returns one vector from matrix
	Vector extractRow(const int index)const;
	//sets value on coordinates and returns reference to matrix 
	Matrix& set(const int row, const int collumn, const double value);
	// returns vector with values made from one column of the matrix
	Vector extractColumn(const int index)const;
	//makes new matrix field with 0 and value on diagonal
	const static Matrix diagonal( double value); 
	//prints the matrix
	void print () const;

};

#endif