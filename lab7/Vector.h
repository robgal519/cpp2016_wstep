#ifndef Vector_h
#define Vector_h

class Vector{
	double _row[3];
public:
	Vector(const double x, const double y, const double z): _row{x,y,z} 
	{};
	//reads value from posision
	double at(const int posicion) const;
	//returns reference to value on posision, which alows to write a value
	double& set(const int posision);
	//prints the vector in row;
	void print()const ;
};

#endif