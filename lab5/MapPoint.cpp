#include "MapPoint.h"
#include <cstring>
#include <iostream>
#include <cmath>
MapPoint::MapPoint(const char* nameData, const double latitude, const double longitude)
{
	this->nameData = new char[strlen(nameData)+1];
	this-> latitude=latitude;
	this->longitude=longitude;
	strcpy(this->nameData,nameData);
}
MapPoint::~MapPoint()
{
	delete [] nameData;
}

void MapPoint::print()const
{
	char north ='N';
	char east = 'E';
	double lat=latitude;
	double lon=longitude;
	if(lat<0)
	{
		lat=(-1)*lat;
		north='S';
	}
	if(lon<0)
	{
		lon=(-1)*lon;
		east='W';
	}
	std::cout<<nameData<<" "<<lat<<north<<" "<<lon<<east<<std::endl;
}
void MapPoint::move(const double x, const double y)
{

	latitude=latitude+x;
	longitude=longitude+y;
}

const MapDist  MapPoint::distance(const MapPoint& A) const
{
	MapDist temp;
	temp.latitude=this->latitude-A.latitude;
	temp.longitude=this->longitude-A.longitude;
	return temp;
}

 MapPoint MapPoint::inTheMiddle(const MapPoint& A,const MapPoint& B, const char * nameData) 
 {
 	const double x=(A.latitude+B.latitude)/2;
 	const double y=(A.longitude+B.longitude)/2;
 	MapPoint temp(nameData,x,y);

 	return temp;
 }

 double MapDist::dist() const
 {
 	return(sqrt(latitude*latitude+longitude*longitude));
 }

const MapPoint& MapPoint::closestFrom (const MapPoint& A,const MapPoint& B) const
{
	const MapDist temp1=distance(A);
	const MapDist temp2=distance(B);

	
	return (temp1.dist()<temp2.dist())?A:B;

}

const char* MapPoint::name()const
{
	return nameData;
}
