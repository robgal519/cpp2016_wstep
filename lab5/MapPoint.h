#ifndef MapPoint_h
#define MapPoint_h

/**
@brief kalasa która przyma różnice długości i sz3erokości geograficznej między dwoma punktami

*/
class MapDist
{
public:

	double latitude;
	double longitude;
	/**
	@fn zwraca odległość pomiędzy dwoma punktami
	*/
	double dist()const;


};
/**
@brief klasa przechowuje informacje o danym punkcie na powierzchni
*/
class MapPoint
{
private:
	double latitude;
	double longitude;
	char* nameData;
public:
	/**
	@fn wypisuje nazwę punktu
	*/
	const char* name()const;
	/**
	@fn konstruktor, wprowadza wszystkie dane
	@arg1 nazwa punktu
	@arg2 szerokość geogr.
	@arg3 długość geogr.	
	*/
	MapPoint(const char* nameData, const double latitude, const double longitude);
	/**
	@fn wypisuje w sformatowanej formie informacje zawarte w obiekcie
	*/
	void print()const;
	/**
	@fn przeuwa dany punkt o zadane wartości
	@arg1 przesunięcie szerokości geogr.
	@arg2 przeunięcie długości geogr.
	*/
	void move(const double x, const double y);

	/**
	@fn funkcja określa który z punktów przekazanych jako argument jest najbliżej
	@return zwraca jeden z argumentów, ten który jest najbliżej
	*/
	const MapPoint& closestFrom (const MapPoint& A,const MapPoint& B) const;

	/**
	@fn funkcja tworzy obiekt typu MapDist określa odległość do punktu A i wypełnia jego pola
	*/
	const MapDist distance(const MapPoint& A)const;
	/**
	@
	*/
	static MapPoint inTheMiddle(const MapPoint& A,const MapPoint& B, const char * nameData);
	~MapPoint();
};



#endif