/* Celem zadania jest napisanie klas MapPoint opisujacej punkt na mapie 
   i opisujaca odleglosc MapDist. 
   
   UWAGA: prosze sprawdzic czy konieczne jest kopiowanie napisow, 
   jesli tak to trzeba to odpowienio zrobic, jesli nie zadanie sie uprosci.
   Przypominam o alokacji pamieci. 

   UWAGA: Mozna uzyc klasy string jak ktos umie.
   
   UWAGA: Wszystkie deklaracje mozna umiescic w jednym naglowku MapPoint.h
  

   Kompilwac do pliku wykonywalnego "test" z opcjami -Wall -g
 */

#include <iostream>
#include "MapPoint.h"
#include "MapPoint.h"

int main() {
  const double latKRK = 50.061389;
  const double lonKRK = 19.938333;
  const MapPoint krk("Krakow", latKRK,  lonKRK);
  krk.print();
  MapPoint nyc("NYC", 40.7127, -74.0059 );
  nyc.print();
  MapPoint porto("Porto", 41.162142, -8.621953 );
  MapPoint near_porto("Near Porto", 41.162142, -8.621953 );
  near_porto.move(0.1, -0.1);
  near_porto.print();
  MapPoint irkutsk("Irkutsk",  52.283333, 104.283333);
  const MapPoint& siberiaCapital = irkutsk;
  siberiaCapital.print();
  
  const MapDist d = nyc.distance(porto);
  std::cout << d.latitude << " " << d.longitude  << " " << d.dist() << std::endl; // odleglosc katawa liczymy ze wzroru Pitagorasa f. hypot 

  const MapPoint& cl = krk.closestFrom( porto, siberiaCapital ); // wybieramy jedna z dwoch punktow zadanych jako arg 1 i 2, taki ktorego odleglosc jest najmniejsza do pierwszego argumentu
  std::cout << cl.name() << std::endl;

  MapPoint mp = MapPoint::inTheMiddle(krk,  siberiaCapital, "Somwhere in Russia"); // punkt w polowie drogi miedzy dwoma przekazanymi w arg 1 i 2
  mp.print();

}
/* wynik
Krakow 50.0614N 19.9383E
NYC 40.7127N 74.0059W
Near Porto 41.2621N 8.72195W
Irkutsk 52.2833N 104.283E
-0.449442 -65.3839 65.3855
Porto
Somwhere in Russia 51.1724N 62.1108E
 */
