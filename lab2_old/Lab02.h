#ifndef Lab02_h
#define Lab02_h

void AddStudent(int*, char ***, char ***, int **, const char *, const char *, char *, int);
void PrintListContent(int, char **);
void PrintListContent(int, int *);
void PrintListContent(int, char **, char **, int *);
void ClearStudents(int *, char ***, char ***, int **);
#endif