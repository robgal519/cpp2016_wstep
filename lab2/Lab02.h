#ifndef Lab02_h
#define Lab02_h 
#include <stdlib.h>
#include <cstring>
#include <stdio.h>
/**
@brief function AddStudent is adding next student to array
	it allocates memory to new arrays, and expand existing arrays
	memory has to be releases!
@return 1 when all alocations and realocations
*/
bool AddStudent(int *,char***, char***, int**, char*, char*,char*,int);

/**
@brief function prints data from char table eg. namesList, surnamesList
@param[1] counter of how many students are sihned in to the list

*/
void PrintListContent(int, char**);
void PrintListContent(int,int*);
void PrintListContent(int, char** , char** , int* );


/**
@brief function deletes all students data
*/
void ClearStudents(int*, char***, char***, int**);

#endif