#include "Lab02.h"
char** mallocChar2D(int count, char ** array)
{
	char** tempArray=(char**)realloc(array, sizeof(char*)*count);
	return tempArray;
}

char* mallocString(unsigned int size)
{
	char* tempVar=(char*) malloc(sizeof(char)*size);
	return tempVar;
}
void add(int studentCounter, char*** array,char* name)
{
	char** ptr=mallocChar2D(studentCounter,*array);

	char* str=mallocString(strlen(name)+2);
	(*array)=ptr;
	((*array)[(studentCounter)-1])=str;
	sprintf((*array)[(studentCounter)-1],"%s",name);
}

void add(int studentCounter, int** array, int year)
{
	(*array)=(int*)realloc((*array),sizeof(int)*(studentCounter));
	((*array)[(studentCounter)-1])=year;

}
bool AddStudent(int * studentCounter,char*** arrayNames, char*** arraySurnames, int** arrayYears, char* firstName, char* lastNane,char* surname,int year)
{
	(*studentCounter)++;

	unsigned int nameLen=strlen(firstName)+strlen(lastNane);
	char* temp=(char*) malloc( sizeof(char)*(nameLen+2));
	sprintf(temp,"%s %s",firstName, lastNane);
	add(*studentCounter,arrayNames,temp);
	free(temp);
	add(*studentCounter,arraySurnames,surname);
	add(*studentCounter,arrayYears,year);

}

void PrintListContent(int studentCounter, char** array)
{
	int tempVar=0;
	for(;tempVar<studentCounter;++tempVar)
	{
		printf("%s\n",array[tempVar]);
	}
}

void PrintListContent(int studentCounter,int* arrayYears)
{
	int tempVar=0;
	for (;tempVar<studentCounter; ++tempVar)
	{
		printf("%d\n",arrayYears[tempVar]);
	}
}

void PrintListContent(int studentCounter, char** arrayNames , char** arraySurnames , int* arrayYears)
{
	int tempVar=0;
	for (;tempVar<studentCounter; ++tempVar)
	{
		printf("%s, %s - year %d\n",arraySurnames[tempVar],arrayNames[tempVar], arrayYears[tempVar]);
	}

}

void ClearStudents(int* studentCounter, char*** arrayNames, char*** arraySurnames, int** arrayYears)
{
for(int i=0;i<(*studentCounter);++i)
	{
		free((*arrayNames)[i]);
		free((*arraySurnames)[i]);
		
	}
	free(*arrayYears);
	free(*arrayNames);
	free(*arraySurnames);

}
