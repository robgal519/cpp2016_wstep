// * Nalezy napisać makefile do kompilacji programu
//
// * Nalezy uzupelnic ciala funkcji tak, aby efekt dzialania programu
// * byl taki jak jest przedstawiony ponizej

#include"Przywitania.h"
#include"Matematyczne.h"

int main ()
{
  PrzywitajSie();
  PrzywitajSiePoAngielsku();
  PrzywitajSieKilkaRazy(Suma(1,2));
}


/* wynik:
Dzien dobry
Good morning
Dzien dobry
Dzien dobry
Dzien dobry
*/
