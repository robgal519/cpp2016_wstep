#include<iostream>
#include"Przywitania.h"

void PrzywitajSie() 
{
  std::cout<<"Dzien dobry"<<std::endl;
}

void PrzywitajSiePoAngielsku()
{
    std::cout << "Good morning\n";
}

void PrzywitajSieKilkaRazy(int count)
{
    while(count--)
    {
         PrzywitajSie();
    }
}

void error(const char * a)
{
		 std::cerr <<a;
}
