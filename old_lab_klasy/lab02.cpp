// Prosze dopisac kod, dodac nowe pliki, tak aby program wykonywal
// sie, a wynik jego dzialania byl taki sam jak podany na końcu tego
// pliku.

// Prosze zaimplementowac klase Wektor, ktora bedzie wektorem o
// dowolnej liczbie wymiarow. Działanie żadnej z funkcji nie może być
// uzależnione od liczby jej wywołań.

// NIE MOZE BYC ZADNYCH WYCIEKOW PAMIECI!

// Pliku lab02.cpp, nie wolno modyfikowac.

// Ostateczny program powinien byc przyjazny dla programisty (miec
// czytelny i dobrze napisany kod). 

// Makefile dolaczony do rozwiazania powinien tworzyc plik wykonywalny
// o nazwie Lab02. Program nalezy kompilowac z flagami -Wall -g.

// Makefile powinien zawierac rowniez cel "clean", ktory usuwa pliki
// obiektowe i plik wykonywalny.

// Przy wykonaniu zadania nie wolno korzystać z internetu, notatek,
// ani żadnych innych materiałów (w tym własnych wcześniej
// przygotowanych plików)

// Kody źródłowe muszą znajdować się w katalogu ~/oop/lab_LABNR. Prawa
// do tego katalogu muszą być równe 700 (tylko dostęp dla
// właściciela).

// Skonczone zadanie nalezy wyslac uruchamiajac skrypt 
// /home/dokt/dog/WyslijZadanie.sh
// bedac w katalogu zawierajacym rozwiazanie czyli ~/oop/lab_02

#include "lab02.h"
#include <iostream>

int main ()
{
  using namespace std;
  
  const int wymiarWektora = 5;
  Wektor x (wymiarWektora);	// tworzy wektor o zadanym wymiarze
  x.Ustaw(0, 1.1).Ustaw(1, 1.2).Ustaw(2, 1.3).Ustaw(3, 1.4).Ustaw(4, 1.5); // ustawia wartosci wspolrzednych wektora
  x.Wypisz();

  // zmiana pierwszej wspolrzednej na 8
  x.Wspolrzedna(0) = 8;
  x.Wypisz();

  // przygotowanie tablicy wspolrzednych wektora
  float *dane = new float[wymiarWektora];
  for (int i = 0; i < wymiarWektora; ++i)
    dane[i] = (float)i/10 + 2;
  const Wektor y (wymiarWektora, dane); // tworzy wektor o zadanym rozmiarze i wartosciach wspolrzednych
  y.Wypisz();

  // zmiana pierwszego elementu tablicy zmienia rowniez wspolrzedna wektora
  dane[0] = 5;
  cout<<"Wspolrzedna 0: "<<y.Wspolrzedna(0)<<"\n";

  // oblicza iloczyn skalarny suma(y_i*x_i)
  cout<<"iloczyn skalarny: "<<y.Iloczyn(x)<<"\n";
  cout<<"iloczyn skalarny: "<<x.Iloczyn(y)<<"\n";

  cout<<"czy x jest const? ";
  y.CzyConst(x);		// wypisuje na ekran "nie"
  cout<<"\n";

  cout<<"czy x jest const dwa? ";
  x.CzyConst(x);		// wypisuje na ekran "nie"
  cout<<"\n";

  cout<<"czy y jest const? ";
  x.CzyConst(y);		// wypisuje na ekran "tak"
  cout<<"\n";

  cout<<"czy y jest const dwa? ";
  y.CzyConst(y);		// wypisuje na ekran "tak"
  cout<<"\n";
}
/* wynik dzialania programu:
(1.1, 1.2, 1.3, 1.4, 1.5)
(8, 1.2, 1.3, 1.4, 1.5)
(2, 2.1, 2.2, 2.3, 2.4)
Wspolrzedna 0: 5
iloczyn skalarny: 52.2
iloczyn skalarny: 52.2
czy x jest const? nie
czy x jest const dwa? nie
czy y jest const? tak
czy y jest const dwa? tak
*/
