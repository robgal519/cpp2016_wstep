#include "StringWrapper.h"
#include <string.h>
#include <iostream>

StringWrapper::StringWrapper(const char* a){
	_data = new char[strlen(a)+1];
	strcpy (_data, a);
}

StringWrapper::StringWrapper(const StringWrapper& a){
	_data = new char[strlen(a.data())+1];
	strcpy(_data, a.data());
}

 int StringWrapper::eq(StringWrapper& a, StringWrapper& b ){
	return !strcmp(a.data(), b.data());
}
 int StringWrapper::eqIcase(StringWrapper& a, StringWrapper& b ){
	return !strcasecmp(a.data(), b.data());
}
StringWrapper& StringWrapper::append(const char * a){
	int sizeAppend=strlen(a)+1;
	char* temp=new char[strlen(_data)+sizeAppend];
	strcpy(temp,_data);
 	strcat(temp,a);
 	delete [] _data;
 	_data = temp;
 	return *this;

}
StringWrapper& StringWrapper::append(StringWrapper& a){
	int sizeAppend=strlen(a.data())+1;
	char* temp=new char[strlen(_data)+sizeAppend];
	strcpy(temp,_data);
 	strcat(temp,a.data());
 	delete [] _data;
 	_data = temp;
 	return *this;
}
const char * StringWrapper::data()const 
{
	return _data;
}
StringWrapper StringWrapper::substring(int a, int b)const
{
	if(b>a)
	{
		char* temp=new char[b-a+1];
		strncpy(temp, _data+a, b-a-1);
		temp[b-a-1]=0;
		StringWrapper siema(temp);
		delete [] temp;
		return siema;
	}

}
StringWrapper::~StringWrapper(){
	delete [] _data;
}


void print(StringWrapper a){
	std::cout<<a.data()<<std::endl;
}
void print(const char* a){
	std::cout<<a<<std::endl;
}
void StringWrapper::print() const {
	std::cout<<_data;
}
