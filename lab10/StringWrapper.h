#ifndef StringWrapper_h
#define StringWrapper_h


class StringWrapper{
	char* _data;
public:
	//konstruktor
	StringWrapper(const char* a);
	//konstruktor kopiujący
	StringWrapper(const StringWrapper& a);

	StringWrapper() {}
	//sprawdza czy ciągi są równe zwraca prawdę jeśli są równe
	static int eq(StringWrapper& a, StringWrapper& b );
	//sprawdza czy ciągi są równe i nie zwraca uwagi na wielkość liter zwraca prawdę gdy są równe
	static int eqIcase(StringWrapper& a, StringWrapper& b );
	//dodaje do pola _data ciąg znaków zawarty w ciągu znaków, lub pobiera go z innego obiektu typu StringWrapper
	StringWrapper& append(const char * a);
	StringWrapper& append(StringWrapper& a);
	//swraca zawartość pola _data
	const char * data()const ;
	//zwraca obiekt typu StringWrapper który zawiera ciąg znaków wycięty z pola _data
	StringWrapper substring(int a, int b)const;
	//destruktor
	~StringWrapper();
	void print() const;
};

void print(StringWrapper a);
void print(const char* a);

#endif