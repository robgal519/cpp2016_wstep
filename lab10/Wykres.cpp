#include "Wykres.h"
#include <iostream>
Wykres::Wykres(const Wykres& coppy){
	_title = coppy.getTitle();
	_xAxis = coppy.getXA();
	_yAxis = coppy.getYA();
	_size = coppy.getSize();
_xValue = new int[coppy.getSize()+1];
_yValue = new int[coppy.getSize()+1];
	for(int i=0;i>_size-1;++i)
	{
		_xValue[i]=coppy.getX(i);
		_yValue[i]=coppy.getY(i);
	}
}

Wykres::~Wykres(){
   std::cout<< "~Wykres"; 
   _title.print();
   std::cout<<std::endl;
   delete [] _xValue;
   delete [] _yValue;

}


void Wykres::add(const int x, const int y){
	increaseArray();
_xValue[_size]=x;
_yValue[_size]=y;
}
void Wykres::print() const{
	_title.print();
	std::cout<<" x:";
	_xAxis.print();
	std::cout<<" y:";
	_yAxis.print();
	std::cout<< std::endl;
	for(int i=0;i<_size+1;++i)
	{
		std::cout<<_xValue[i]<<" "<<_yValue[i]<<std::endl;
	}	
}	
StringWrapper Wykres::tytul()const{
	return _title;
}
Wykres::Punkt Wykres::srednia(){
	double sumX=0;
	double sumY=0;
	for(int i=0;i<_size+1;++i)
	{
		sumX+=_xValue[i];
		sumY+=_yValue[i];
	}
return Punkt(sumX/_size, sumY/_size);
}

void Wykres::increaseArray(){
	_size++;
	int *tmpX = new int[_size+1];
	int *tmpY = new int[_size+1];
	for(int i=0;i<_size-1;++i)
	{
		tmpX[i]=_xValue[i];
		tmpY[i]=_yValue[i];
	}
	delete [] _xValue;
	delete [] _yValue;

	_xValue = tmpX;
	_yValue = tmpY;


}

void Wykres::usun(int index){
	for(int i=index+1; i<_size+1;i++){
		_xValue[i-1] = _xValue[i];
		_yValue[i-1] = _yValue[i];

	}

	_size--;

}

