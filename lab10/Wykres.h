#ifndef Wykres_h
#define Wykres_h
#include "StringWrapper.h"


class Wykres
{
StringWrapper _title;
StringWrapper _xAxis;
StringWrapper _yAxis;

int* _xValue;
int* _yValue;
int _size;

public:
	Wykres(const char* title, const char* xAxis, const char* yAxis):
		_title(title),
		_xAxis(xAxis),
		_yAxis(yAxis),
		_size(0),
		_xValue(0),
		_yValue(0)
	{}
	Wykres(const Wykres& coppy);
    ~Wykres();

class Punkt{
	double _x;
	double _y;
public:
	Punkt(double x, double y):
	_x(x),_y(y){}

	const double x() const{
		return _x;
	}
	const double y() const{
		return _y;
	}
};

	void add(const int x, const int y);
	void print() const;	
	StringWrapper tytul()const;
	Punkt srednia();
	void increaseArray();
	int getX(int index) const{
		return _xValue[index];
	}
	int getY(int index) const{
		return _yValue[index];
	}
	int getSize() const{
		return _size;
	}
    StringWrapper getTitle() const {
    	return _title;
    }

    StringWrapper getXA() const {
    	return _xAxis;
    }

    StringWrapper getYA() const {
    	return _yAxis;
    }
 void usun(int index);

};

#endif